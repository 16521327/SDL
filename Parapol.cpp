#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
//area 1
	int P = 1 - A;
	int x = 0;
	int y = 0;
	Draw2Points(xc,yc, x,y,ren);
	while (x < A)
	{
		if (P <= 0) P += 2 * x + 3;
		else {
			P += 2 * x + 3 - 2*A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	
//area 2
	P = 2 * A - 1;
	while (x<200 && x>-200)
	{
		if (P <= 0) P += 4 * A;
		else {
			P += 4 * A - 4 * x - 4;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int P = 1 - A;
	int x = 0;
	int y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (P <= 0) P += 2 * x + 3;
		else {
			P += 2 * x + 3 - 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	P = 2 * A - 1;
	while (x<200 && x>-200)
	{
		if (P <= 0) P += 4 * A;
		else {
			P += 4 * A - 4 * x - 4;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
