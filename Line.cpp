#include "Line.h"
//DDA algorithm
void DDA_Line1(int dx, int dy, float m, float x, float y, int x2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	while (x < x2)
	{
		x = x + 1;
		y = y + m;
		SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	}
}

void DDA_Line2(int dx, int dy, float m, float x, float y, int x2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	while (x > x2)
	{
		x = x - 1;
		y = y - m;
		SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	}
}
void DDA_Line3(int dx, int dy, float dm, float x, float y, int y2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	while (y<y2)
	{
		x = x + dm;
		y = y + 1;
		SDL_RenderDrawPoint(ren, int(x+0.5), int(y ));
	}
}
void DDA_Line4(int dx, int dy, float dm, float x, float y, int y2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, int(x), int (y+0.5));
	while (y > y2)
	{
		x = x - dm;
		y = y - 1;
		SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	}

}
void DDA_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int dx = x2 - x1;
	int dy = y2 - y1;
	float m = (float)dy / dx;
	float dm = (float)dx / dy;
	float x = x1;
	float y = y1;

	//1: 0 < m < 1 & dx > 0; -1 < m < 0 & dx > 0
	if ((m > 0 && m <= 1 && dx > 0) || (m >= -1 && m < 0 && dx > 0))
	{
		DDA_Line1(dx, dy, m, x, y, x2, ren);
	}
	//2: 0 < m < 1 & dx < 0; -1 < m < 0 & dx < 0
	else if ((m > 0 && m <= 1 && dx < 0) || (m >= -1 && m < 0 && dx < 0))
	{
		DDA_Line2(dx, dy, m, x, y, x2, ren);
	}
	//3: m > 1 & dy > 0; m < -1 & dy > 0
	else if ((m > 1 && dy > 0) || (m < -1 && dy > 0))
	{
		DDA_Line3(dx, dy, dm, x, y, y2, ren);
	}
	//4: m > 1 & dy < 0; m < -1 & dy < 0
	else if ((m > 1 && dy < 0) || (m < -1 && dy < 0))
	{
		DDA_Line4(dx, dy, dm, x, y, y2, ren);
	}

}

void DDA_Line_ver2(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	float dx = x2 - x1;
	float dy = y2 - y1;
	int s;
	if (abs(dx) >= abs(dy)) s = abs(dx); else s = abs(dy);
	float x_inc = dx / s;
	float y_inc = dy / s;
	float x = x1;
	float y = y1;
	SDL_RenderDrawPoint(ren, int(x+0.5), int(y+0.5));
	int k = 1;
	while(k<=s)
	{
		x = x + x_inc;
		y = y + y_inc;
		k++;
		SDL_RenderDrawPoint(ren, int(x+0.5 ), int(y+0.5 ));
	}
}

//Bresenham algorithm
//Case 1: 0 < m <= 1 & dx > 0
void Bresenham_Line1(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	int c1 = 2 * dy;
	int c2 = 2 * dy - 2 * dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (x < x2)
	{
		if (p <= 0) 
		{
			p = p + c1;
		}
		else 
		{
			p = p + c2;
			y = y + 1;
		}
		x = x + 1;
		SDL_RenderDrawPoint(ren, x , y);
	}

}
//Case 2: 0 < m <= 1 & dx < 0
void Bresenham_Line2(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (x > x2)
	{
		if (p <= 0) {
			p += 2 * dy;
		}
		else {
			p += 2 * dy - 2 * dx;
			y = y - 1;
		}
		x = x - 1;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 3: m > 1 & dy > 0
void Bresenham_Line3(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (y < y2)
	{
		if (p <= 0) {
			p += 2 * dx;
		}
		else {
			p += 2 * dx - 2 * dy;
			x = x + 1;
		}
		y = y + 1;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 4: m > 1 & dy < 0
void Bresenham_Line4(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (y > y2)
	{
		if (p <= 0) {
			p += 2 * dx;
		}
		else {
			p += 2 * dx - 2 * dy;
			x = x - 1;
		}
		y = y - 1;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 5: -1 < m < 0 & dx > 0
void Bresenham_Line5(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (x < x2)
	{
		if (p <= 0) {
			p += 2 * dy;
		}
		else {
			p += 2 * dy - 2 * dx;
			y = y - 1;
		}
		x = x + 1;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 6: -1 < m < 0 & dx < 0
void Bresenham_Line6(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (x > x2)
	{
		if (p <= 0) {
			p += 2 * dy;
		}
		else {
			p += 2 * dy - 2 * dx;
			y = y + 1;
		}
		x = x - 1;
		SDL_RenderDrawPoint(ren, x, y);
	}

}
//Case 7: m < -1 & dy > 0
void Bresenham_Line7(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (y < y2)
	{
		if (p <= 0) {
			p += 2 * dx;
		}
		else {
			p += 2 * dx - 2 * dy;
			x = x - 1;
		}
		y = y + 1;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 8: m < -1 & dy < 0
void Bresenham_Line8(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (y > y2)
	{
		if (p <= 0) {
			p += 2 * dx;
		}
		else {
			p += 2 * dx - 2 * dy;
			x = x + 1;
		}
		y = y - 1;
		SDL_RenderDrawPoint(ren, x, y);
	}
}

void Bresenham_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int dx = x2 - x1;
	int dy = y2 - y1;
	float m = (float)dy / dx;
	int x = x1;
	int y = y1;


	if (m > 0 && m <= 1 && dx > 0) {
		Bresenham_Line1(dx, dy, x, y, x2, ren);
	}
	else if (m > 0 && m <= 1 && dx < 0) {
		Bresenham_Line2(dx, dy, x, y, x2, ren);
	}
	else if (m > 1 && dy > 0) {
		Bresenham_Line3(dx, dy, x, y, y2, ren);
	}
	else if (m > 1 && dy < 0) {
		Bresenham_Line4(dx, dy, x, y, y2, ren);
	}
	else if (m < 0 && m >= -1 && dx > 0) {
		Bresenham_Line5(dx, dy, x, y, x2, ren);
	}
	else if (m < 0 && m >= -1 && dx < 0) {
		Bresenham_Line6(dx, dy, x, y, x2, ren);
	}
	else if (m < -1 && dy > 0) {
		Bresenham_Line7(dx, dy, x, y, y2, ren);
	}
	else if (m < -1 && dy < 0) {
		Bresenham_Line8(dx, dy, x, y, y2, ren);
	}
}



void Bresenham_Line_ver2(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int dx = x2 - x1;
	int dy = y2 - y1;
	int x = x1; int y = y1;
	int lineLength;
	int P;
	int c1, c2;
	if (abs(dx) >= abs(dy))
	{
		lineLength = abs(dx);
		P = 2 * abs(dy) - abs(dx);
		c1 = 2 * abs(dy);
		c2 = 2 * abs(dy) - 2 * abs(dx);
	}
	else
	{
		lineLength = abs(dy);
		P = 2 * abs(dx) - abs(dy);
		c1 = 2 * abs(dx);
		c2 = 2 * abs(dx) - 2 * abs(dy);
	}

	int k = 1;
	SDL_RenderDrawPoint(ren, x, y);
	while(k < lineLength)
	{
		if (P <= 0) P = P + c1;
		else
		{
			P = P + c2;
			if (abs(dx) >= abs(dy))
				if (dy > 0) y = y + 1; else y = y - 1;
			else if (dx > 0) x = x + 1; else x = x - 1;
		}

		if (abs(dx) >= abs(dy)) if (dx > 0) x = x + 1; else x = x - 1;
		else if (dy > 0) y = y + 1; else y = y - 1;

		SDL_RenderDrawPoint(ren, x, y);
		k = k + 1;
	}
}

//Midpoint algorithm
//Case 1: 0 < m < 1 & dx > 0
void Midpoint_Line1(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	int const1 = 2 * dy - 2 * dx;
	int const2 = 2 * dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (x < x2) {
		if (p <= 0) {
			p += const2;
		}
		else {
			p += const1;
			y=y+1;
		}
		x=x+1;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 2: 0 < m < 1 & dx < 0
void Midpoint_Line2(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	int const1 = 2 * dy - 2 * dx;
	int const2 = 2 * dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (x > x2) {
		if (p <= 0) {
			p += const2;
		}
		else {
			p += const1;
			y--;
		}
		x--;
		SDL_RenderDrawPoint(ren, x, y);
	}

}
//Case 3: m > 1 & dy > 0
void Midpoint_Line3(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	int const1 = 2 * dx - 2 * dy;
	int const2 = 2 * dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (y<y2) 
	{
		if (p <= 0)
		{
			p += const2;
		}
		else 
		{
			p += const1;
			x++;
		}
		y++;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 4: m > 1 & dy < 0
void Midpoint_Line4(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	int const1 = 2 * dx - 2 * dy;
	int const2 = 2 * dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (y > y2)
	{
		if (p <= 0) {
			p += const2;
		}
		else {
			p += const1;
			x--;
		}
		y--;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 5: -1 < m < 0 & dx > 0
void Midpoint_Line5(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	int const1 = 2 * dy - 2 * dx;
	int const2 = 2 * dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (x < x2) {
		if (p <= 0) {
			p += const2;
		}
		else {
			p += const1;
			y--;
		}
		x++;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 6: -1 < m < 0 & dx < 0
void Midpoint_Line6(int dx, int dy, int x, int y, int x2, SDL_Renderer *ren)
{
	int p = 2 * dy - dx;
	int const1 = 2 * dy - 2 * dx;
	int const2 = 2 * dy;
	SDL_RenderDrawPoint(ren, x, y);
	while (x > x2) {
		if (p <= 0) {
			p += const2;
		}
		else {
			p += const1;
			y++;
		}
		x--;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 7: m < -1 & dy > 0
void Midpoint_Line7(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	int const1 = 2 * dx - 2 * dy;
	int const2 = 2 * dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (y < y2)
	{
		if (p <= 0) {
			p += const2;
		}
		else {
			p += const1;
			x--;
		}
		y++;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
//Case 8: m < -1 & dy < 0
void Midpoint_Line8(int dx, int dy, int x, int y, int y2, SDL_Renderer *ren)
{
	int p = 2 * dx - dy;
	int const1 = 2 * dx - 2 * dy;
	int const2 = 2 * dx;
	SDL_RenderDrawPoint(ren, x, y);
	while (y > y2)
	{
		if (p <= 0) {
			p += const2;
		}
		else {
			p += const1;
			x++;
		}
		y--;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
void Midpoint_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int dx = x2 - x1;
	int dy = y2 - y1;
	int x = x1;
	int y = y1;
	float m = (float)dy / dx;

	if (m > 0 && m <= 1 && dx > 0)
		Midpoint_Line1(dx, dy, x, y, x2, ren);
	else if (m > 0 && m <= 1 && dx < 0)
		Midpoint_Line2(dx, dy, x, y, x2, ren);
	else if (m > 1 && dy > 0)
		Midpoint_Line3(dx, dy, x, y, y2, ren);
	else if (m > 1 && dy < 0)
		Midpoint_Line4(dx, dy, x, y, y2, ren);
	else if (m < 0 && m >= -1 && dx > 0)
		Midpoint_Line5(dx, dy, x, y, x2, ren);
	else if (m < 0 && m >= -1 && dx < 0)
		Midpoint_Line6(dx, dy, x, y, x2, ren);
	else if (m < -1 && dy > 0)
		Midpoint_Line7(dx, dy, x, y, y2, ren);
	else if (m < -1 && dy < 0)
		Midpoint_Line8(dx, dy, x, y, y2, ren);

}

void Midpoint_Line_ver2(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int dx = x2 - x1;
	int dy = y2 - y1;
	int x = x1; int y = y1;
	int lineLength;
	int P;
	int c1, c2;
	if (abs(dx) >= abs(dy))
	{
		lineLength = abs(dx);
		P = 2 * abs(dy) - abs(dx);
		c1 = 2 * abs(dy);
		c2 = 2 * abs(dy) - 2 * abs(dx);
	}
	else
	{
		lineLength = abs(dy);
		P = 2 * abs(dx) - abs(dy);
		c1 = 2 * abs(dx);
		c2 = 2 * abs(dx) - 2 * abs(dy);
	}

	int k = 1;
	SDL_RenderDrawPoint(ren, x, y);
	while (k < lineLength)
	{
		if (P <= 0) P = P + c1;
		else
		{
			P = P + c2;
			if (abs(dx) >= abs(dy))
				if (dy > 0) y = y + 1; else y = y - 1;
			else if (dx > 0) x = x + 1; else x = x - 1;
		}

		if (abs(dx) >= abs(dy)) if (dx > 0) x = x + 1; else x = x - 1;
		else if (dy > 0) y = y + 1; else y = y - 1;

		SDL_RenderDrawPoint(ren, x, y);
		k = k + 1;
	}
}
