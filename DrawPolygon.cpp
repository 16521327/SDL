#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 3;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = PI/2;

	int x[vertice];
	int y[vertice];
	for (int i = 0; i < vertice; i++)
	{
		x[i] = xc +int( R*cos(PHI)+0.5);
		y[i] = yc -int( R*sin(PHI)+0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], x[(i + 1) % vertice], y[(i + 1) % vertice], ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 4;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = PI / 4;

	int x[vertice];
	int y[vertice];
	for (int i = 0; i < vertice; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], x[(i + 1) % vertice], y[(i + 1) % vertice], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 5;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = PI / 2;

	int x[vertice];
	int y[vertice];
	for (int i = 0; i < vertice; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], x[(i + 1) % vertice], y[(i + 1) % vertice], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 6;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = PI / 2;

	int x[vertice];
	int y[vertice];
	for (int i = 0; i < vertice; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], x[(i + 1) % vertice], y[(i + 1) % vertice], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 5;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = PI / 2;

	int x[vertice];
	int y[vertice];
	for (int i = 0; i < vertice; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], x[(i + 2) % vertice], y[(i + 2) % vertice], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 5;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = PI / 2;
	double r = sin(PI / 10)*R / sin(7 * PI / 10);
	double PHInho = PHI + PI / 5;

	int x[vertice];
	int y[vertice];
	int xnho[vertice];
	int ynho[vertice];
	for (int i = 0; i < vertice;i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);

		xnho[i] = xc+ int(r*cos(PHInho)+0.5);
		ynho[i] = yc - int(r*sin(PHInho)+0.5);
		PHI += INC_PHI;
		PHInho += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], xnho[i],ynho[i] , ren);
		Bresenham_Line_ver2(xnho[i], ynho[i], x[(i + 1)%vertice], y[(i + 1)%vertice], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 8;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = PI / 2;
	double r = sin(PI / 8)*R / sin(6 * PI / 8);
	double PHInho = PHI + PI / 8;

	int x[vertice];
	int y[vertice];
	int xnho[vertice];
	int ynho[vertice];
	for (int i = 0; i < vertice; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);

		xnho[i] = xc +int( r*cos(PHInho)+0.5);
		ynho[i] = yc - int(r*sin(PHInho)+0.5);
		PHI += INC_PHI;
		PHInho += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], xnho[i], ynho[i], ren);
		Bresenham_Line_ver2(xnho[i], ynho[i], x[(i + 1) % vertice], y[(i + 1) % vertice], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	double const PI = 3.1412;
	int const vertice = 5;
	double const INC_PHI = 2 * PI / vertice;
	double PHI = startAngle;
	double r = sin(PI / 10)*R / sin(7 * PI / 10);
	double PHInho = PHI + PI / 5;

	int x[vertice];
	int y[vertice];
	int xnho[vertice];
	int ynho[vertice];
	for (int i = 0; i < vertice; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);

		xnho[i] = xc + int(r*cos(PHInho)+0.5);
		ynho[i] = yc - int(r*sin(PHInho)+0.5);
		PHI += INC_PHI;
		PHInho += INC_PHI;
	}
	for (int i = 0; i < vertice; i++)
	{
		Bresenham_Line_ver2(x[i], y[i], xnho[i], ynho[i], ren);
		Bresenham_Line_ver2(xnho[i], ynho[i], x[(i + 1) % vertice], y[(i + 1) % vertice], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	const double PI = 3.1412;
	double phi = PI / 2;
	while (r > 1)
	{
		DrawStarAngle(xc, yc, r,phi, ren);
		r = sin(PI / 10)*r / sin(7 * PI / 10);
		phi += PI;
	}
}
