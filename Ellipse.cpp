#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0;
	int y = b;
	int p = a * a + 2 * b*b - 2 * a*a*b;
	Draw4Points(xc, yc, x, y, ren);
	while (x<=a*a/sqrt(a*a+b*b))
	{
		if (p <= 0)
			p += 4 * b*b*x + 6 * b*b;
		else
		{
			p += 4 * b*b*x - 4 * a*a*y + 6 * b*b + 4 * a*a;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = a;
	y = 0;
	p = b * b + 2 * a*a - 2 * a*b*b;
	Draw4Points(xc, yc, x, y, ren);
	while (x>a*a / sqrt(a*a + b*b))
	{
		if (p <= 0)
			p += 4 * a*a*y + 6 * a*a;
		else
		{
			p += 4 * a*a*y - 4 * b*b*x + 6 * a*a + 4 * b*b;
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0;
	int y = b;
	float p = b * b - a * a * b + a * a / 4;
	Draw4Points(xc, yc, x, y, ren);
	while (x <= a*a / sqrt(a*a + b*b))
	{
		if (p <= 0)
			p += 2 * b*b*x + 3 * b*b;
		else
		{
			p += 2 * b*b*x - 2 * a*a*y + 2 * a*a + 3 * b*b;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = a;
	y = 0;
	int p2 = a * a - a * b*b + b * b / 4;
	Draw4Points(xc, yc, x, y, ren);
	while (x>a*a / sqrt(a*a + b*b))
	{
		if (p2 <= 0)
			p2 += 2 * a*a*y + 3 * a*a;
		else
		{
			p2 += -2 * b*b*x + 2 * a*a*y + 2 * b*b + 3 * a*a;
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
}
