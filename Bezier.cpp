#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	for (float i = 0; i <= 1; i = i + 0.001)
	{
		float x = (1 - i)*(1 - i)*p1.x + 2 * i*(1 - i)*p2.x + i*i*p3.x;
		float y = (1 - i)*(1 - i)*p1.y + 2 * i*(1 - i)*p2.y + i*i*p3.y;
		SDL_RenderDrawPoint(ren, int(x + 0.5), int(y + 0.5));
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	for (float i = 0; i <= 1; i = i + 0.001)
	{
		float x = (1 - i)*(1 - i)*(1 - i)*p1.x + 3 * i* (1 - i)* (1 - i)*p2.x + 3 * i*i*(1 - i)*p3.x + i* i*i*p4.x;
		float y = (1 - i)*(1 - i)*(1 - i)*p1.y + 3 * i* (1 - i)* (1 - i)*p2.y + 3 * i*i*(1 - i)*p3.y + i* i*i*p4.y;
		SDL_RenderDrawPoint(ren, int(x + 0.5), int(y + 0.5));
	}
}
