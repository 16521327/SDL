#ifndef LineH
#define LineH
#include <SDL.h>
#include<cmath>
void DDA_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
void DDA_Line_ver2(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
void Bresenham_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
void Bresenham_Line_ver2(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
void Midpoint_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
void Midpoint_Line_ver2(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
#endif
