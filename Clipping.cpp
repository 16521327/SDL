#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if (c1 != 0 && c2 != 0 && c1&c2 != 0)
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	Q2 = P2;
	Q1 = P1;
	int c1 = Encode(r, Q1);
	int c2 = Encode(r, Q2);
	if (CheckCase(c1,c2) == 1) return 0;
	while (CheckCase(c1,c2) == 3)
	{
		if (Encode(r, Q1) == 0)
		{
			Vector2D temp = Q2;
			Q2 = Q1;
			Q1 = temp;
		}
		ClippingCohenSutherland(r, Q1, Q2);
		c1 = Encode(r, Q1);
		c2 = Encode(r, Q2);
	}
	if (CheckCase(c1, c2) == 2)
	{
		Q1 = { 0,0 };
		Q2 = { 0, 0 };
	}
	return 0;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	float m = dy / dx;
	float dm = dx / dy;
	int x=P1.x, y=P2.y;
	if (Encode(r, P1) & LEFT)
	{
		x = r.Left;
		y = P1.y + m *(x - P1.x);
	}
	else if (Encode(r, P1)&RIGHT )
	{
		x = r.Right;
		y = P1.y + m *(x - P1.x);
	}
	else if (Encode(r, P1) & TOP)
	{
		y = r.Top;
		x = P1.x + dm*(y - P1.y);
	}
	else if (Encode(r, P1) &BOTTOM)
	{
		y = r.Bottom;
		x = P1.x + dm*(y - P1.y);
	}
	P1.x = x;
	P1.y = y;
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t<t1)
			return 0;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
	t1 = t;
	return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1=0, t2=1;
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	int xmin = r.Left;
	int xmax = r.Right;
	int ymin = r.Top;
	int ymax = r.Bottom;
	int a = SolveNonLinearEquation(-dx, P1.x - xmin, t1, t2);
	int b = SolveNonLinearEquation(dx, xmax - P1.x, t1, t2);
	int c = SolveNonLinearEquation(-dy, P1.y - ymin, t1, t2);
	int d = SolveNonLinearEquation(dy, ymax - P1.y, t1, t2);
	if (a*b*c*d)
	{
		Q1.x = P1.x + t1*dx;
		Q1.y = P1.y + t1*dy;
		Q2.x = P1.x + t2*dx;
		Q2.y = P1.y + t2*dy;
	}
	return 0;
}
